function getLinesFromFile(path) {
  return new Promise((resolve, reject) => {
    try {
      const lineReader = require('readline').createInterface({
        input: require('fs').createReadStream(path),
      });
  
      const lines = [];
      lineReader
      .on('line', (line) => lines.push(line))
      .on('close', () => resolve(lines));
    } catch (err) {
      reject(err);
    }    
  });
}

exports.configFromPath = async function configFromPath(path) {
  const lines = await getLinesFromFile(path);

  return lines
    .filter((line) => !/^\s*?#/.test(line))
    .map((line) => line.split("=").map((s) => s.trim()))
    .reduce((config, [k, v]) => {
      config[k] = v;
      return config;
    }, {});
};